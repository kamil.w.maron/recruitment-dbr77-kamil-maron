<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## komendy do uruchomienia serwera:.

Pierwsze ueuchomienie:<br />
Upewnij się że masz na systemie zainstalowane [composer oraz php](https://www.geeksforgeeks.org/how-to-install-php-composer-on-windows/).<br />
Następnie w katalogu z repozytorium uruchom terminal i użyj następujących komend:<br />
<code>cp .env.example .env </code><br />
<code>composer require laravel/sail --dev && php artisan sail:install </code><br />
<code>./vendor/bin/sail up -d</code><br />
<code>./vendor/bin/sail php artisan migrate </code><br />
<code>./vendor/bin/sail php artisan key:generate</code><br />
<code>./vendor/bin/sail npm install </code><br />
<code>./vendor/bin/sail npm run dev </code><br /><br />

Aby włączyć serwer każdym kolejnym razem użyj tej komendy:<br />
<code>./vendor/bin/sail up -d && ./vendor/bin/sail npm run dev</code><br /><br />
Aby wyłączyć serwer, w terminalu w katalogu repozytorium użyj tej komendy:<br />
<code>./vendor/bin/sail down</code><br /><br />
Aby zrestartować serwer(np. jakieś pliki/komponenty nie chcą się przeładować) w terminalu w katalogu repozytorium użyj tej komendy:<br />
<code>./vendor/bin/sail down && ./vendor/bin/sail up -d && ./vendor/bin/sail npm run dev</code>

[Live Preview](http://rudflix.unixstorm.eu/)
